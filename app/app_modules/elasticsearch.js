// Fetch base env variables for elasticsearch configuration.
const utils = require('./utils.js')
const uuidv4 = require('uuid/v4');

var host = utils.env("ES_HOST", "http://localhost");
var host_port = utils.env("ES_PORT", "9200");
var require_auth = utils.env("ES_AUTH_REQUIRED", false);

var unescape = require('unescape-js');

const Cryptr = require('cryptr');
const cryptr = new Cryptr(utils.env("SECRET_KEY", "svxp8CaSpWGVD5yRWftmdwwazyzXGvsu7fRb3txQJFn6dU6SDGZwmnJTTvBsByNT"));

var ready = false;

// Parse the ES_AUTH_REQUIRED paramenter to check if yes/true were actually strings.
if (require_auth === "yes" || require_auth === "true" || require_auth === true) {
    require_auth = true;
} else {
    require_auth = false;
}

var protocol = "http";
if (host.includes("://")) {
    protocol = host.split("://")[0];
    host = host.split("://")[1];
}
host = host.replace(/\/$/, "");

// Fetch auth paramenters if ES_AUTH_REQUIRED is set to true.
var auth = "";
if (require_auth) {
    auth = utils.env("ES_AUTH_USERNAME", "elastic") + ":" + utils.env("ES_AUTH_PASSWORD", "elastic") + "@";
}
auth = protocol + "://" + auth;

// Connnect to the elasticsearch node.
const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: encodeURI(auth + host + ":" + host_port) });

const indeces = [
    "night-owl-settings",
    "night-owl-log",
    "night-owl-devices",
    "night-owl-configs",
    "night-owl-credentials",
    "night-owl-users"
]

function base() {
    indeces.forEach(index => {
        client.indices.exists({
            index: index,
        },
            (err, resp) => {
                if (!resp.body) {
                    client.indices.create({ index: index });
                }
            })
    });
}

// "Public" Functions.
module.exports = {
    // Night Owl Settings and logging.
    initialize: function () {
        base();
    },
    settings_get: function (var_name, callback) {
        client.search({
            body: {
                "query": {
                    "terms": {
                        "_id": [var_name]
                    }
                }
            }
        },
            (err, resp) => {
                try {
                    callback(cryptr.decrypt(resp.body.hits.hits[0]._source.doc.value))
                } catch (err) {
                    callback(null)
                }
            }
        );
    },
    settings_put: function (var_name, value, callback = null) {
        var enc_var = cryptr.encrypt(value);
        client.index({
            index: "night-owl-settings",
            refresh: true,
            id: var_name,
            type: "json",
            body: {
                doc: {
                    value: enc_var
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    log_get: function (source, callback) {

    },
    log_put: function (source, log, callback) {

    },
    // Device related information and logs.
    device_get: function (device_name, callback) {
        if (device_name === "*") {
            client.search({
                index: "night-owl-devices",
                size: 10000,
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        items = []
                        for (var i = 0; i < resp.body.hits.hits.length; i++) {
                            resp.body.hits.hits[i]._source.doc.name = resp.body.hits.hits[i]._id
                            if (resp.body.hits.hits[i]._source.doc) {
                                items.push(resp.body.hits.hits[i]._source.doc)
                            }                            
                        }
                        callback(items);
                    } catch (err) {
                        console.log("[e] Error fetching device list: " + err);
                        callback(null);
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-devices",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        resp.body.hits.hits[0]._source.doc.name = resp.body.hits.hits[0]._id;
                        callback(resp.body.hits.hits[0]._source.doc)
                    } catch (err) {
                        callback(null)
                    }
                }
            );
        }

    },
    device_put: function (device_name, ip, make, details, type, credential, group = null, callback = null) {
        if(group){
            console.log("[i] Device tagged with group: " + group);
        }else{
            group = "default";
        }
        if(credential){
            console.log("[i] Device tagged with group: " + credential);
        }else{
            credential = "default";
        }
        client.index({
            index: "night-owl-devices",
            refresh: true,
            id: device_name,
            type: "json",
            body: {
                doc: {
                    ip: ip,
                    make: make,
                    details: details,
                    type: type,
                    credential: credential,
                    group: group
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    device_delete: function (device_name) {
        try {
            client.deleteByQuery({
                index: "night-owl-devices",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                function () {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }

    },
    config_get: function (device_name, callback) {
        client.search({
            index: "night-owl-configs",
            body: {
                "query": {
                    "terms": {
                        "doc.device.keyword": [device_name]
                    }
                }
            }
        },
            (err, resp) => {
                try {
                    var hits = resp.body.hits.hits;
                    var configs = [];
                    for(var i = 0; i < hits.length; i++) {
                        var hit_object = hits[i]._source
                        configs.push(
                            {
                                "timestamp": hit_object.timestamp,
                                "config": unescape(hit_object.doc.config)
                            }
                        )
                    }
                    callback(configs);
                } catch (err) {
                    console.log("[e] Error parsing config list: " + err);
                    callback(null);
                }
            }
        );
    },
    config_put: function (device_name, config, callback) {
        client.index({
            index: "night-owl-configs",
            refresh: true,
            id: uuidv4(),
            type: "json",
            body: {
                timestamp: (new Date()).toUTCString(),
                doc: {
                    device: device_name,
                    config: config.replace(/\r\n|\n\r|\n|\r/g, '\n')
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    config_delete: function (config_id) {
        try {
            client.deleteByQuery({
                index: "night-owl-configs",
                body: {
                    "query": {
                        "terms": {
                            "_id": [config_id]
                        }
                    }
                }
            },
                function () {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }
    },
    credentials_get: function (cred_name, callback) {
        if (cred_name === "*") {
            client.search({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        items = []
                        for (var i = 0; i < resp.body.hits.hits.length; i++) {
                            items.push(resp.body.hits.hits[i]._id);
                        }
                        callback(items);
                    } catch (err) {
                        callback(null)
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        "terms": {
                            "_id": [cred_name]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        try {
                            callback({
                                "username": cryptr.decrypt(resp.body.hits.hits[0]._source.doc.username),
                                "password": cryptr.decrypt(resp.body.hits.hits[0]._source.doc.password)
                            })
                        } catch (err) {
                            callback(null)
                        }
                    } catch (err) {
                        callback(null)
                    }
                }
            );
        }
    },
    credentials_delete: function (cred_name) {
        try {
            client.deleteByQuery({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        "terms": {
                            "_id": [cred_name]
                        }
                    }
                }
            },
                function () {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }
    },
    credentials_put: function (cred_name, username, password, callback = null) {
        username = cryptr.encrypt(username);
        password = cryptr.encrypt(password);
        client.index({
            index: "night-owl-credentials",
            refresh: true,
            id: cred_name,
            type: "json",
            body: {
                doc: {
                    username: username,
                    password: password
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    log_put: function (source, category, src, dst, log_data, callback = null) {
        client.index({
            index: "night-owl-logs",
            refresh: true,
            id: uuidv4(),
            type: "json",
            body: {
                timestamp: (new Date()).toUTCString(),
                doc: {
                    instance: source,
                    log: log_data,
                    src: src,
                    dst: dst,
                    category: category
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
}