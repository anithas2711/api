const es = require("../elasticsearch.js");


var super_groups = [
    "Acs_NetAdmin_GlobalIT"
]

var groups = [
    "Acs_NetAdmin_Hult",
    "Acs_NetAdmin_Language"
]

const root_config = `
#!/usr/local/sbin/tac_plus
id = spawnd {
    listen = { address = 0.0.0.0 port = 49 }
    spawn = {
            instances min = 1
            instances max = 10
    }
    background = no
}

id = tac_plus {
    debug = MAVIS

    access log = /var/log/tac_plus/access/%Y/%m/access-%m-%d-%Y.txt
    accounting log = /var/log/tac_plus/accounting/%Y/%m/accounting-%m-%d-%Y.txt
    authentication log = /var/log/tac_plus/authentication/%Y/%m/authentication-%m-%d-%Y.txt

    mavis module = external {
        script out = {
            if (undef($TACMEMBER) && $RESULT == ACK) set $RESULT = NAK
            if ($RESULT == ACK) set $PASSWORD_ONESHOT = 1
        }
        setenv LDAP_SERVER_TYPE = "microsoft"
        setenv LDAP_HOSTS = "ldap://ldap.ef.com"
        setenv LDAP_BASE = "DC=ef,DC=com"
        setenv LDAP_SCOPE = sub
        setenv LDAP_FILTER = "(&(objectClass=user)(objectClass=person)(sAMAccountName=%s))"
        setenv LDAP_USER = "svc.ansible@ef.com"
        setenv LDAP_PASSWD = "dfXw53r4paKp"
        setenv AD_GROUP_PREFIX = ""
        setenv UNLIMIT_AD_GROUP_MEMBERSHIP = "1"
        setenv EXPAND_AD_GROUP_MEMBERSHIP = 0
        setenv REQUIRE_TACACS_GROUP_PREFIX = 0
        setenv FLAG_USE_MEMBEROF = 1
        exec = /usr/local/lib/mavis/mavis_tacplus_ldap.pl
    }

    login backend = mavis
    user backend = mavis
    connection timeout = 600
    context timeout = 3600
    password max-attempts = 1
    password backoff = 1
    separation tag = "*"
    skip conflicting groups = yes
    skip missing groups = yes
    user backend = mavis
    login backend = mavis chpass
    pap backend = mavis

    # Global host group.
    host = world {
        address = 0.0.0.0/0
        prompt = "Welcome to EF GlobalIT TACACS Services, please log in to continue."
        key = "2KRSTRGQCfcxvNp28YHDBn7dE8ffBnLS"
    }
    {{host_list}}
}
`;

var host_list = [
    {
        "ip": "10.39.44.50",
        "make": "cisco",
        "group": "Acs_NetAdmin_Language",
        "details": {
            "model": "WS-C3750X-24",
            "serial-number": "FDO1546P14Q",
            "os-version": "15.0(2)SE6",
            "hostname": "CHLZ-MP01",
            "last_scrape": "Wed, 31 Jul 2019 16:46:31 GMT"
        },
        "type": "ios",
        "credential": "default",
        "name": "CHLZ-MP01"
    },
    {
        "ip": "10.24.32.8",
        "make": "cisco",
        "group": "Acs_Netadmin_Hult",
        "details": {
            "model": "WS-C3750X-24P",
            "serial-number": "FDO1817Z08X",
            "os-version": "15.0(2)SE6",
            "hostname": "GBLC-HLT-ES04",
            "last_scrape": "Thu, 08 Aug 2019 08:46:03 GMT"
        },
        "type": "ios",
        "credential": "default",
        "name": "GBLC-HLT-ES04"
    },
    {
        "ip": "10.43.180.1",
        "make": "fortinet",
        "group": "Acs_Netadmin_Hult",
        "details": {},
        "type": "fortios",
        "credential": "default",
        "name": "USSF-HLT-FW01-Primary"
    }
]

var dynamic_config = `
    # Network Host Definitions.`;
host_list.forEach(host => {
    dynamic_config += `
    host = `+host.name+` {
        address = "`+host.ip+`"
        template = world
    }
    `;
});

var group_config = `
    # Group Access Permissions.`;
    group_config+= `
    group = default {
        message = "Welcome! Your access mode is set to 'Read-Only'."`;
    host_list.forEach(host => {
        if (true) {
            group_config += `
        server = permit `+host.name+``;
        }
    });
    group_config += `
        server = deny 0.0.0.0/0`;

    group_config += `
        default service = permit
        # IOS
        service = shell {
            set priv-lvl = 5
            default attribute = permit
            default cmd = permit
        }
        # WLC
        service = ciscowlc {
            set role1 = MONITOR
        }
        # Fortigate
        service = fortigate {
            optional admin_prof = read_only
        }
    }`;

super_groups.forEach(group => {
    group_config+= `
    group = `+group+` {
        message = "Welcome SuperUser! Your access mode is set to 'Read-Write'."`;
    host_list.forEach(host => {
        if (true) {
            group_config += `
        server = permit `+host.name+``;
        }
    });
    group_config += `
        server = deny 0.0.0.0/0`;

    group_config += `
        default service = permit
        # IOS
        service = shell {
            set priv-lvl = 15
            default attribute = permit
            default cmd = permit
        }
        # WLC
        service = ciscowlc {
            set role1 = ALL
        }
        # Fortigate
        service = fortigate {
            optional admin_prof = super_admin
        }
    }`;
});

groups.forEach(group => {
    group_config+= `
    group = `+group+`_WRITE {
        message = "Welcome! Your access mode is set to 'Read-Write'."`;
    host_list.forEach(host => {
        if (host.group === group) {
            group_config += `
        server = permit `+host.name+``;
        }
    });
    group_config += `
        server = deny 0.0.0.0/0`;

    group_config += `
        default service = permit
        # IOS
        service = shell {
            set priv-lvl = 15
            default attribute = permit
            default cmd = permit
        }
        # WLC
        service = ciscowlc {
            set role1 = ALL
        }
        # Fortigate
        service = fortigate {
            optional admin_prof = super_admin
        }
    }
    group = `+group+` {
        member = default
        member = `+group+`_WRITE
    }
    `;
});

dynamic_config += group_config;
var out_config = root_config.replace("{{host_list}}", dynamic_config);
console.log(out_config);

function replaceAll(source_string, old_text, new_text) {
    while (source_string.includes(old_text)){
        source_string = source_string.replace(old_text, new_text);
    }
    return source_string;
}